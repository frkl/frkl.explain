#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_explain` package."""

import frkl.explain
import pytest  # noqa


def test_assert():

    assert frkl.explain.get_version() is not None
