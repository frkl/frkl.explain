# -*- coding: utf-8 -*-
from typing import List

from frkl.common.doc import Doc
from frkl.common.formats.serialize import to_value_string
from rich.markdown import Markdown


def doc_to_table_rows(doc: Doc) -> List:
    rows = []

    desc = doc.get_help(default=None)
    if desc:
        md = Markdown(desc)
        rows.append(["desc", md])

    for k, v in doc.metadata.items():
        if hasattr(v, "__rich__") or hasattr(v, "__rich_console__"):
            value_string = v
        else:
            value_string = to_value_string(v)
        rows.append([k, value_string])

    return rows
