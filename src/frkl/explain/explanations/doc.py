# -*- coding: utf-8 -*-
from typing import Any, Iterable, List, Mapping, Optional, Union

from frkl.common.async_utils import wrap_async_task
from frkl.common.doc import Doc
from frkl.common.formats.serialize import to_value_string
from frkl.explain.explanation import DataExplanation
from rich import box
from rich.console import Console, ConsoleOptions, RenderResult
from rich.markdown import Markdown
from rich.table import Table


FULL_INFO_TEMPLATE = """[key]{{ name }}[/key]: [italic]{{ short_help }}[/italic]

{% if desc %}  [key]desc[/key]: [italic]{{ desc | wordwrap(_terminal_width-10) | replace('\n', '\n        ') }}[/italic]

{% endif %}{% for k,v in metadata.items() %}  [key]{{ k }}[/key]
{% endfor %}
"""


class InfoExplanation(DataExplanation):
    def __init__(
        self,
        data: Any,
        name: Optional[str] = None,
        only_slug: bool = False,
        short_help_key: Optional[str] = None,
        help_key: Optional[str] = None,
        show_title: bool = True,
    ):

        if name is None:
            name = "n/a"

        self._name = name
        super().__init__(data=data)

        self._info: Optional[Doc] = None
        self._info_data_raw: Union[Doc, Mapping[str, Any], str, None] = None

        self._short_help_key: Optional[str] = short_help_key
        self._help_key: Optional[str] = help_key

        self._only_slug: bool = only_slug
        self._show_title: bool = show_title

    @property
    def name(self) -> str:
        return self._name

    async def get_info(self) -> Doc:

        if self._info is None:

            self._info = Doc(
                self.data, short_help_key=self._short_help_key, help_key=self._help_key
            )
        return self._info

    async def create_explanation_data(self) -> Mapping[str, Any]:

        info = await self.get_info()
        return info.exploded_dict()

    @property
    def full_info(self) -> bool:
        return self._only_slug

    @full_info.setter
    def full_info(self, full_info: bool) -> None:
        self._only_slug = full_info

    @property
    def show_title(self) -> bool:
        return self._show_title

    @show_title.setter
    def show_title(self, show_title: bool) -> None:
        self._show_title = show_title

    def full_info_table_rows(self, info: Doc) -> List:
        rows = []

        desc = info.get_help(default=None)
        if desc:
            md = Markdown(desc)
            rows.append(["desc", md])

        for k, v in info.metadata.items():
            if hasattr(v, "__rich__") or hasattr(v, "__rich_console__"):
                value_string = v
            else:
                value_string = to_value_string(v)
            rows.append([k, value_string])

        return rows

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:

        info_data: Doc = wrap_async_task(self.get_info)
        if self._only_slug:
            yield f"[key2]{self.name}[/key2]: [value]{info_data.get_short_help('-- no information --')}"
            return

        if self.show_title:
            short_help = info_data.get_short_help(default=None)
            if short_help:
                short_help = f" [value]({short_help})[/value]"
            else:
                short_help = ""
            yield f" [underline][title]{self.name}[/title][/underline]{short_help}"

        table = Table(show_header=False, box=box.SIMPLE, padding=(0, 2, 0, 0))
        table.add_column("Property", style="key2", no_wrap=True)
        table.add_column("Value", style="value")

        for row in self.full_info_table_rows(info=info_data):
            table.add_row("", "")
            table.add_row(*row)

        yield table


class InfoListExplanation(DataExplanation):
    def __init__(self, *info_items: InfoExplanation, full_info: bool = False):

        super().__init__(data=info_items)
        self._full_info = full_info

    @property
    def full_info(self) -> bool:
        return self._full_info

    @full_info.setter
    def full_info(self, full_info: bool) -> None:
        self._full_info = full_info

    async def create_explanation_data(self) -> Mapping[str, Any]:

        raise NotImplementedError()

    def __rich_console__(
        self, console: Console, options: ConsoleOptions
    ) -> RenderResult:

        info_items: Iterable[InfoExplanation] = self.data
        if not self._full_info:
            table = Table(show_header=False, box=box.SIMPLE)
            table.add_column("name", style="key2")
            table.add_column("desc", style="value")

            for item in info_items:

                table.add_row(item.name, item.explanation_data["short_help"])

            yield table
            return

        table = Table(show_header=False, box=box.MINIMAL, show_lines=True)
        table.add_column("Name", no_wrap=True, style="title")
        table.add_column("Details")

        for info in info_items:
            info.show_title = False
            info.full_info = True

            # info_data = info.explanation_data
            raise NotImplementedError()

            # slug = (
            #     " [italic]"
            #     + info_data.get_short_help("-- no description available --")
            #     + "[/italic]"
            # )
            #
            # i_table = Table(show_header=False, box=box.SIMPLE)
            # i_table.add_column("Property", style="key2", no_wrap=True)
            # i_table.add_column("Value", style="value")
            # for row in info.full_info_table_rows():
            #     i_table.add_row(*row)
            #
            # cell = []
            # cell.append(slug)
            # cell.append(i_table)
            #
            # table.add_row(info.name, RenderGroup(*cell))

        yield table
