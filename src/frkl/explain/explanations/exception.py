# -*- coding: utf-8 -*-
import textwrap
from typing import TYPE_CHECKING, Any, List, Mapping

from frkl.common.cli import get_terminal_size
from frkl.common.exceptions import FrklException, ensure_frkl_exception
from frkl.common.strings import reindent
from frkl.common.types import isinstance_or_subclass
from frkl.explain.explanation import DataExplanation


if TYPE_CHECKING:
    from rich.console import ConsoleOptions, Console, RenderResult


class ExceptionExplanation(DataExplanation):
    async def create_explanation_data(self) -> Mapping[str, Any]:

        return {"exception": self.data}

    def create_exception_text(self) -> List[str]:
        """Pretty prints an exception to the terminal."""

        lines = []
        frkl_exc: FrklException = ensure_frkl_exception(self.data)

        cols = self.config_value("width", None)
        if cols is None:
            cols, _ = get_terminal_size()

        # TODO: fix first line
        msg = f"[red bold]Error:[/red bold] {frkl_exc.msg}"
        for m in msg.split("\n"):
            m = textwrap.fill(m, width=cols, subsequent_indent="       ")
            lines.append(m)
        lines.append("")

        if frkl_exc.reason:
            lines.append("  [bold]Reason:[/bold]")
            msg = reindent(frkl_exc.reason, 4)

            for m in msg.split("\n"):
                m = textwrap.fill(m, width=cols, subsequent_indent="    ")
                lines.append(m)
            lines.append("")

        elif frkl_exc.parent is not None:
            lines.append("  [bold]Parent exception:[/bold]")
            if isinstance_or_subclass(frkl_exc.parent, FrklException):
                parent_exc_string = frkl_exc.parent.message_short  # type: ignore
            else:
                parent_exc_string = str(frkl_exc.parent)

            msg = reindent(parent_exc_string, 4)
            lines.append(msg)
            lines.append("")
        if frkl_exc.solution:
            lines.append("  [bold]Solution:[/bold]")
            msg = reindent(frkl_exc.solution, 4)
            for m in msg.split("\n"):
                m = textwrap.fill(m, width=cols, subsequent_indent="    ")
                lines.append(m)
            lines.append("")
        if frkl_exc.references:
            if len(frkl_exc.references) == 1:
                url = frkl_exc.references[list(frkl_exc.references.keys())[0]]
                lines.append("  [bold]Reference:[/bold] " + url)
            else:
                lines.append("  [bold]References:[/bold]")
                for k, v in frkl_exc.references.items():
                    lines.append(f"    {k}: [italic]{v}[/italic]")

        lines.append("")

        return lines

    def __rich_console__(
        self, console: "Console", options: "ConsoleOptions"
    ) -> "RenderResult":

        return self.create_exception_text()
