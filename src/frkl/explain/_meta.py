# -*- coding: utf-8 -*-
from typing import Any, Dict


project_name = "frkl.explain"
project_main_module = "frkl.explain"
project_slug = "frkl_explain"

pyinstaller: Dict[str, Any] = {}
