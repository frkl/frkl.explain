# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from typing import TYPE_CHECKING, Any, Mapping, Optional, Type

import dpath.util
from frkl.common.async_utils import wrap_async_task
from frkl.common.exceptions import FrklException
from frkl.common.formats.serialize import make_serializable, to_value_string
from frkl.common.types import isinstance_or_subclass


if TYPE_CHECKING:
    from rich.console import ConsoleOptions, Console, RenderResult


class Explanation(metaclass=ABCMeta):
    def __init__(self, **config: Any):

        self._config: Mapping[str, Any] = config
        self._explanation_data: Optional[Mapping[str, Any]] = None

    def config(self) -> Mapping[str, Any]:
        return self._config

    def config_value(self, key: str, default=None) -> Any:

        return self._config.get(key, default)

    async def _load(self) -> None:

        expl_old = self._explanation_data
        if expl_old:
            raise NotImplementedError()

        self._explanation_data = await self.create_explanation_data()

    @property
    def explanation_data(self) -> Mapping[str, Any]:

        if self._explanation_data is None:
            wrap_async_task(self._load)
        return self._explanation_data  # type: ignore

    @property
    def serializable_data(self) -> Mapping[str, Any]:

        return make_serializable(self.explanation_data)

    def __str__(self):

        return to_value_string(self.serializable_data)

    def __repr__(self):

        data_str = str(self.explanation_data)[0:24] + "..."
        return f"{self.__class__.__name__}(data={data_str})"

    def explain(self, key_path: str, default: Any = None) -> Any:

        try:
            value: Any = dpath.util.get(self.explanation_data, key_path, separator=".")
            return value
        except KeyError:
            return default
        return None

    @abstractmethod
    async def create_explanation_data(self) -> Mapping[str, Any]:
        pass

    def __rich_console__(
        self, console: "Console", options: "ConsoleOptions"
    ) -> "RenderResult":

        val_string = to_value_string(self.serializable_data)
        yield val_string


class DataExplanation(Explanation):
    def __init__(self, data: Any, data_cls: Optional[Type] = None, **config: Any):

        if data_cls:
            if not isinstance_or_subclass(data, data_cls):
                raise FrklException(
                    msg="Can't create explanation object for data.",
                    reason=f"Data is of different type ({type(data)}) then expected ({data_cls}).",
                )

        self._data: Any = data
        self._data_cls: Optional[Type] = data_cls
        super().__init__(**config)

    @property
    def data(self) -> Any:

        return self._data

    # @data.setter
    # def data(self, data: Any):
    #
    #     self._data = data
    #     self._explanation_data = None


class SimpleDataExplanation(DataExplanation):
    def __init__(self, data: Any, data_name: "str" = "data", **kwargs):

        self._data_name: str = data_name
        super().__init__(data, **kwargs)

    async def create_explanation_data(self) -> Mapping[str, Any]:

        return {self._data_name: self._data}
