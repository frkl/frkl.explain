# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


frkl_explain_app_dirs = AppDirs("frkl_explain", "frkl")

if not hasattr(sys, "frozen"):
    FRKL_EXPLAIN_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `frkl_explain` module."""
else:
    FRKL_EXPLAIN_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "frkl_explain"  # type: ignore
    )
    """Marker to indicate the base folder for the `frkl_explain` module."""

FRKL_EXPLAIN_RESOURCES_FOLDER = os.path.join(
    FRKL_EXPLAIN_MODULE_BASE_FOLDER, "resources"
)
